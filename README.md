# Monroe MCAR Realtor Scraper

Monroe MCAR Realtor web scraper written in [Python](https://www.python.org) using [Jupyter](https://www.jupyter.org) notebook.  Refer to [Web Scraping with Python for a Friend]( https://ejstembler.com/2015/12/web-scraping-with-python-for-a-friend) for the story behind this project.
